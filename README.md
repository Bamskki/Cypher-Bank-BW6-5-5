# Cypher Bank - The New Way of Using Bitcoin

**APP UNDER CONSTRUCTION. FIGMA DESIGN AND PROTOTYPE CAN BE FOUND [HERE](https://www.figma.com/file/tOPzd8F5sS5vux6o8gquhx/Prototype-1?type=design&node-id=48%3A114&mode=design&t=RM6JJskEqcuRwQpG-1)**

Derived from BlueWallet & Blink Wallet

Built with React Native, Electrum, and Blink's API from Galoy. You could think of Blink as bank, and Galoy as a factory that creates banks.

Read BlueWallet's [repo](https://github.com/BlueWallet/BlueWallet/)

Galoy [Read me](https://github.com/GaloyMoney/galoy)

Blink Developer [Documentation](https://dev.blink.sv/)

Website: [cypherbank.io](cypherbank.io)

Community: [telegram group](https://t.me/+t01zpIzF5s8wYjlh)

* Private keys never leave your device
* Lightning Network supported through Blink's API
* SegWit-first. Replace-By-Fee support
* Encryption. Plausible deniability
* Gamified experience 
* And many more features...

Blink Account registration process: Users should register their Blink accounts directly from this [page]( https://consent.blink.sv/login?login_challenge=0b48a0faa424456a9b198ba1b15d7179)
and then it will redirect to our callback URL upon completion.

<img src="https://i.imgur.com/bJtx6Cg.png" width="100%">